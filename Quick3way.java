import java.util.Scanner;

/**
 * Created by Nguyet on 10/27/15.
 */
public class Quick3way {
    private Quick3way(){ }
    public static void sort(String[]a ){
        sort(a, 0, a.length-1);
        assert isSorted(a);
    }
    private static void sort(String[]a, int lo, int hi){
        if(hi<=lo)         return;
        String v = a[lo];
        int lt=lo, gt=hi;
        int i=lo;
        while(i<=gt){
            int cmp = a[i].compareTo(v);
            if(cmp<0)    exch(a, lt++, i++);
            else if(cmp>0)   exch(a, i, gt--);
            else i++;
        }
        sort(a, lo, lt-1);
        sort(a, gt+1, hi);
        assert isSorted(a);
    }
    private static void exch(Object[]a, int i, int j){
        Object swap = a[i];
        a[i]=a[j];
        a[j]= swap;
    }
    private static boolean isSorted(String[]a){
       for(int i=1;i<a.length; i++ )
           if(less(a[i],a[i-1]))  return false;
        return true;
    }
    public static boolean less(String v, String w){
        return v.compareTo(w)<0;
    }
    private static void show(String[]a){
        for(int i=0; i<a.length; i++)
            System.out.println(a[i] + " ");

    }
    public static void main(String[]args){
        Scanner input = new Scanner(System.in);
        int i, n;
        String a[];
        System.out.println("nhap so luong phan tu trong day la:");
        n=input.nextInt();
        a=new String[n];
        System.out.println("nhap day can sap xep la:");
        for(i=0; i<n; i++)
            a[i]=input.nextLine();
        System.out.println("day da nhap la:");
        for(i=0; i<a.length; i++)
            System.out.println(a[i] + " ");
        Quick3way.sort(a);
        System.out.println();
        System.out.println("day da duoc sap xep la:");
        show(a);

    }
}
