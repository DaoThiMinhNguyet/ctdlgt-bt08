

import java.util.Scanner;

/**
 * Created by Nguyet on 10/22/15.
 */
public class QuickSort {
    private QuickSort(){}
    public static void sort(int []a){
        sort(a, 0, a.length-1);
       // assert isSorted(a);
    }
    private static void sort(int[]a, int lo, int hi){
        if(hi<=lo)  return;
        int j=partition(a, lo , hi);
        sort(a, lo, j-1);
        sort(a, j + 1, hi);
       // assert isSorted(a);
    }
    private static int partition(int[] a, int lo, int hi){
        int i=lo;
        int j=hi+1;
        int v=a[lo];
        while(true){
            while(a[++i]<v) {
                if (i == hi) break;
            }
            while(v<a[--j]) {
                if (j == lo) break;
            }
            if(i>= j)       break;
            exch(a, i, j);
        }
        exch(a, lo, j);
        return j;
    }
    private static void exch(int[]a, int i, int j){
        int swap=a[i];
        a[i]=a[j];
        a[j]=swap;
    }
   /* private static boolean isSorted (int[]a){
        for(int i=1; i<a.length; i++)
            if(a[i]<a[i-1])  return false;
        return true;
    }*/
    private static void show(int[]a){
        for(int i=0; i<a.length; i++){
            System.out.println(a[i] + " ");
        }
    }
    public static void main(String[] args){
        int i, n, lo, hi;
        int []a;
        Scanner input =new Scanner(System.in);
        System.out.println("nhap n:");
        n=input.nextInt();
        a=new int[n];
        System.out.println("nhap day can sap xep la:");
        for(i=0; i<n; i++)
            a[i]=input.nextInt();
        System.out.println("day da nhap la:");
        for(i=0; i<n; i++)
            System.out.println(a[i] + " ");
        QuickSort.sort(a);
        System.out.println();
        System.out.println("day da duoc sap xep la:");
        show(a);
    }
}
