import java.util.Scanner;

/**
 * Created by Nguyet on 10/28/15.
 */
public class bai4b {
    public static void main(String[] args){
        Scanner input=new Scanner(System.in);
        int i, n;
        int[]a;
        System.out.print("nhap n:");
        n=input.nextInt();
        a=new int[2*n+1];
        System.out.print("nhap day can sap xep la:");
        for(i=0; i<a.length; i++)
            a[i]=input.nextInt();
        System.out.println("day da nhap la:");
        for(i=0; i<a.length; i++)
            System.out.println(a[i] + " ");
        System.out.println();
        sort(a);
        System.out.println("trung vi cua day la:" + a[(a.length / 2)]);
    }
    public static int partition(int[]a, int lo, int hi){
        int i=lo;
        int j=hi +1;
        int v=a[lo];
        while(true){
            while(a[++i]<v) {
                if (i == hi) break;
            }
            while(a[--j]>v) {
                if (j == lo) break;
            }
            if(i>=j)              break;
            exch(a,i,j);
        }
        exch(a, lo, j);
        return(j);
    }
    public static void sort(int[]a){
        sort(a, 0, a.length - 1);
    }
    public static void sort(int[]a, int lo, int hi){
        if(lo>=hi)       return;
        int j=partition(a, lo, hi);
        sort(a, lo, j-1);
        sort(a, j + 1, hi);
    }
    public static void exch(int[]a, int i, int j){
        int swap=a[i];
        a[i]=a[j];
        a[j]=swap;
    }
}
